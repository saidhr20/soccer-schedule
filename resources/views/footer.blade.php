<footer class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 text-center" >
                <div class="widget mb-3">
                    <h3>Tournois</h3>
                    <ul class="list-unstyled links">
                        <li><a href="{{route('teams')}}">les equipes</a></li>
                        <li><a href="{{route('players')}}">buteurs</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-4 text-center">
                <div class="widget mb-3">
                    <h3>Matches</h3>
                    <ul class="list-unstyled links">
                        <li><a href="{{route('groupe')}}">classement des groupes</a></li>
                        <li><a href="{{route('matchs')}}">Matchs prochaines</a></li>
                        <li><a href="{{route('matchs')}}">Résultas</a></li>

                    </ul>
                </div>
            </div>

            <div class="col-lg-4 text-center">
                <div class="widget mb-3">
                    <h3>Social</h3>
                    <ul class="list-unstyled links">
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Instagram</a></li>
                        <li><a href="#">Youtube</a></li>
                    </ul>
                </div>
            </div>

        </div>

        <div class="row text-center">
            <div class="col-md-12">
                <div class="pt-5">
                    <p>
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with
                        <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">
                            Colorlib</a>
                    </p>
                </div>
                    <p>
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | Tournois at vugharden was devlopped by <a href="https://bylka.net">SAID</a>
                    </p>
            </div>

        </div>
    </div>
</footer>