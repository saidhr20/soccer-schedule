@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.groupe.title')</h3>
    @can('groupe_create')
    <p>
        <a href="{{ route('admin.groupe.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($groupe) > 0 ? 'datatable' : '' }} @can('groupe_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('groupe_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>ID </th>
                        <th>Groupe Name</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($groupe) > 0)
                        @foreach ($groupe as $group)
                            <tr data-entry-id="{{ $group->id }}">
                                @can('groupe_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $group->id}}</td>
                                <td>{{ $group->name or '' }}</td>
                                <td>
                                    @can('groupe_view')
                                    <a href="{{ route('admin.groupe.show',[$game->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('groupe_edit')
                                    <a href="{{ route('admin.groupe.edit',[$game->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('groupe_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.groupe.destroy', $game->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('groupe_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.groupe.mass_destroy') }}';
        @endcan

    </script>
@endsection