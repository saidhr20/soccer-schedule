@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.players.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.players.fields.team')</th>
                            <td>{{ $player->team->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.players.fields.name')</th>
                            <td>{{ $player->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.players.fields.surname')</th>
                            <td>{{ $player->surname }}</td>
                        </tr>
                        <tr>
                            <th>buts</th>
                            <td>{{ $player->buts}}</td>
                        </tr>
                        <tr>
                            <th>assists</th>
                            <td>{{ $player->assist}}</td>
                        </tr>
                        <tr>
                            <th>cartons jaune</th>
                            <td>{{ $player->carton_jaune}}</td>
                        </tr>
                        <tr>
                            <th>cartons rouge</th>
                            <td>{{ $player->carton_rouge}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.players.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop