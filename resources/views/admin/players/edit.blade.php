@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.players.title')</h3>
    
    {!! Form::model($player, ['method' => 'PUT', 'route' => ['admin.players.update', $player->id]]) !!}
    {!! csrf_field() !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('team_id', 'Team', ['class' => 'control-label']) !!}
                    {!! Form::select('team_id', $teams, old('team_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('team_id'))
                        <p class="help-block">
                            {{ $errors->first('team_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Nom', 'Nom', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Prénom', 'Prénom', ['class' => 'control-label']) !!}
                    {!! Form::text('surname', old('surname'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('surname'))
                        <p class="help-block">
                            {{ $errors->first('surname') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Buts', 'Buts', ['class' => 'control-label']) !!}
                    {!! Form::number('buts', old('buts'), ['class' => 'form-control number', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('buts'))
                        <p class="help-block">
                            {{ $errors->first('buts') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Passe décisif', 'Passe décisif', ['class' => 'control-label']) !!}
                    {!! Form::number('assist', old('assist'), ['class' => 'form-control number', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('assist'))
                        <p class="help-block">
                            {{ $errors->first('assist') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Carton jaune', 'Carton jaune', ['class' => 'control-label']) !!}
                    {!! Form::number('carton_jaune', old('carton_jaune'), ['class' => 'form-control number', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('carton_jaune'))
                        <p class="help-block">
                            {{ $errors->first('carton_jaune') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Carton Rouge', 'Carton Rouge', ['class' => 'control-label']) !!}
                    {!! Form::number('carton_rouge', old('carton_rouge'), ['class' => 'form-control number', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('carton_rouge'))
                        <p class="help-block">
                            {{ $errors->first('carton_rouge') }}
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
    <script>
        $('.date').datepicker({
            autoclose: true,
            dateFormat: "{{ config('app.date_format_js') }}"
        });
    </script>

@stop