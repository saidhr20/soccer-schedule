<!DOCTYPE html>
<html lang="en">

<head>
    <title>Les groupes</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">



</head>

<body>

<div class="site-wrap">

    @include('header')

    <div class="hero overlay" style="background-image: url('images/bg_3.jpg');">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 ml-auto">
                    <h1 class="text-white text-center">TOURNOIS <br/>AT VUƔARDEN</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section py-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-6 title-section ">
                    <h2 class="heading">Groupes</h2>
                </div>
                <div class="col-6 text-right">
                    <div class="custom-nav">
                        <a href="#" class="js-custom-prev-v2"><span class="icon-keyboard_arrow_left"></span></a>
                        <span></span>
                        <a href="#" class="js-custom-next-v2"><span class="icon-keyboard_arrow_right"></span></a>
                    </div>
                </div>
            </div>


            <div class="owl-4-slider owl-carousel">
                @foreach($groupes as $groupe)
                    <div class="item">
                        <div class="col-6 title-section  text-center mb-3 m-auto">
                            <h1 class="site-section-heading text-white mb-5">Groupe "{{$groupe->name}}" </h1>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="widget-next-match table-responsive">
                                    <table class="table custom-table">
                                        <thead>
                                        <tr>

                                            <th>Team</th>
                                            <th>P</th>
                                            <th>W</th>
                                            <th>D</th>
                                            <th>L</th>
                                            <th>PTS</th>
                                        </tr>
                                        </thead>
                                        @foreach($groupe->team()->get()->sortByDesc('points') as $team)
                                            <tbody>
                                            <tr>

                                                <td><a href="{{route('equipe.show',$team->id)}}"><strong class="text-white">{{$team->name}}</strong></a></td>
                                                <td>{{ $team->played }}</td>
                                                <td>{{ $team->won }}</td>
                                                <td>{{ $team->tied }}</td>
                                                <td>{{ $team->lost }}</td>
                                                <td>{{ $team->points }}</td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>



    @include('footer')



</div>
<!-- .site-wrap -->

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.mb.YTPlayer.min.js"></script>


<script src="js/main.js"></script>

</body>

</html>