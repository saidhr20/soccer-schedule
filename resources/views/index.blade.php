<!DOCTYPE html>
<html lang="en">

<head>
    <title>Tournois At vugarden</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">



</head>

<body>

<div class="site-wrap">

    @include('header')

    <div class="hero overlay" style="background-image: url('{{asset('images/bg_0.jpg')}}');">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 ml-auto">
                    <h1 class="text-white text-center">TOURNOIS <br/>AT VUƔARDEN</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container py-5">
            <div class="row">
                @foreach($nextmatch as $game)
                    <div class="col-lg-12 m-auto">
                        <div class="widget-next-match">
                            <div class="widget-title  text-center">
                                <h2>Prochaine  match</h2>
                            </div>

                            <div class="widget-body mb-1">
                                <div class="widget-vs">
                                    <div class="d-flex align-items-center justify-content-around justify-content-between w-100">
                                        <div class="team-1 text-center">
                                            <img src="images/logo_1.png" alt="Image">
                                            <h3>{{ $game->team1->name }}</h3>
                                        </div>
                                        <div>
                                            <span class="vs"><span>VS</span></span>
                                        </div>
                                        <div class="team-2 text-center">
                                            <img src="images/logo_2.png" alt="Image">
                                            <h3>{{ $game->team2->name }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center widget-vs-contents mb-1">
                                <p class="mb-2">
                                    <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('d-m-Y') }}</span>
                                    <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('H:i') }}</span>
                                </p>
                            </div>

                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div> <!-- .site-section -->

    <div class="container teams result">
        <div class="col-12 title-section">
            <h2 class="heading">Derniers matchs</h2>
        </div>
        <div class="row">
            @foreach($results as $game)
            <div class="col-lg-6 m-auto">
                <div class="d-flex team-vs">
                    <span class="score">{{ $game->result1 }} - {{ $game->result2 }}</span>
                    <div class="team-1 w-50">
                        <div class="team-details w-100 text-center">
                            <img src="images/logo_1.png" alt="Image" class="img-fluid">
                            <h3>{{ $game->team1->name }}</h3>
                        </div>
                    </div>
                    <div class="team-2 w-50">
                        <div class="team-details w-100 text-center">
                            <img src="images/logo_2.png" alt="Image" class="img-fluid">
                            <h3>{{ $game->team2->name }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="latest-news mb-3">
        <div class="container">
            <div class="row">
                <div class="col-12 title-section">
                    <h2 class="heading">Photos</h2>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-md-4">
                    <div class="post-entry">
                        <a href="#">
                            <img src="{{asset('images/img_5.jpg')}}" alt="Image" class="img-fluid">
                        </a>
                     </div>
                </div>
                <div class="col-md-4">
                    <div class="post-entry">
                        <a href="#">
                            <img src="{{asset('images/img_6.jpg')}}" alt="Image" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="post-entry">
                        <a href="#">
                            <img src="{{asset('images/img_4.jpg')}}" alt="Image" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="site-section mb-5 py-5">
        <div class="container">
            <div class="row">
                <div class="col-6 title-section">
                    <h2 class="heading">Groupes</h2>
                </div>
                <div class="col-6 text-right">
                    <div class="custom-nav">
                        <a href="#" class="js-custom-prev-v2"><span class="icon-keyboard_arrow_left"></span></a>
                        <span></span>
                        <a href="#" class="js-custom-next-v2"><span class="icon-keyboard_arrow_right"></span></a>
                    </div>
                </div>
            </div>


            <div class="owl-4-slider owl-carousel">
                @foreach($groupes as $groupe)
                <div class="item">
                    <div class="col-6 title-section  text-center mb-3 m-auto">
                        <h1 class="site-section-heading text-white mb-5">Groupe "{{$groupe->name}}" </h1>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="widget-next-match">
                                <div class="widget-title">
                                    <h3>Prochaines matches</h3>
                                </div>
                                @foreach($groupe->game()->whereNull('result1')->get() as $game)
                                <div class="widget-body">
                                    <div class="widget-vs">
                                        <div class="d-flex align-items-center justify-content-around justify-content-between w-100">
                                            <div class="team-1 text-center">
                                                <img src="images/logo_1.png" alt="Image" class="img-fluid" width="75">
                                                <h3>{{ $game->team1->name}}</h3>
                                            </div>
                                            <div class="text-center">
                                                <span class="vs"><span>VS</span></span>
                                                <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('Y-m-d') }}</span>
                                                <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('H:i') }}</span>
                                            </div>
                                            <div class="team-2 text-center">
                                                <img src="images/logo_2.png" alt="Image"  width="75">
                                                <h3>{{ $game->team2->name}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="text-center widget-vs-contents mb-4">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="widget-next-match table-responsive">
                                <table class="table custom-table">
                                    <thead>
                                    <tr>

                                        <th>Team</th>
                                        <th>P</th>
                                        <th>W</th>
                                        <th>D</th>
                                        <th>L</th>
                                        <th>PTS</th>
                                    </tr>
                                    </thead>
                                     @foreach($groupe->team()->get()->sortByDesc('points') as $team)
                                    <tbody>
                                    <tr>

                                        <td><strong class="text-white">{{$team->name}}</strong></td>
                                        <td>{{ $team->played }}</td>
                                        <td>{{ $team->won }}</td>
                                        <td>{{ $team->tied }}</td>
                                        <td>{{ $team->lost }}</td>
                                        <td>{{ $team->points }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>



    @include('footer')



</div>
<!-- .site-wrap -->

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.mb.YTPlayer.min.js"></script>


<script src="js/main.js"></script>

</body>

</html>