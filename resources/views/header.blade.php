<div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close">
            <span class="icon-close2 js-menu-toggle"></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>


<header class="site-navbar py-4" role="banner">

    <div class="container">
        <div class="d-flex align-items-center">
            <div class="site-logo">
                <a href="{{route('index')}}">
                    <img src="{{asset('images/logo.png')}}" alt="Logo">
                </a>
            </div>
            <div class="ml-auto">
                <nav class="site-navigation position-relative text-right" role="navigation">
                    <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                        <li class="{{ (request()->is('games')) ? 'active' : '' }}"><a href="{{route('index')}}" class="nav-link">Acceuil</a></li>
                        <li class="{{ (request()->is('Matchs')) ? 'active' : '' }}"><a href="{{route('matchs')}}" class="nav-link">Matches et Résultats</a></li>
                        <li class="{{ (request()->is('groupe')) ? 'active' : '' }}"><a href="{{route('groupe')}}" class="nav-link">Classement et Groupes</a></li>
                        <li class="{{ (request()->is('Players')) ? 'active' : '' }}"><a href="{{route('players')}}" class="nav-link">Buteurs</a></li>
                        <li class="{{ (request()->is('teams')) ? 'active' : '' }}"><a href="{{route('teams')}}" class="nav-link">Equipes</a></li>
                    </ul>
                </nav>

                <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black float-right text-white"><span
                            class="icon-menu h3 text-white"></span></a>
            </div>
        </div>
    </div>

</header>