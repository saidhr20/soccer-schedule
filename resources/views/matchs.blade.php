<!DOCTYPE html>
<html lang="en">

<head>
    <title>Matchs et résultas</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta content="Tournois">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">



</head>

<body>

<div class="site-wrap">

@include('header')

    <div class="hero overlay" style="background-image: url('{{asset('images/bg_3.jpg')}}');"w>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 mx-auto text-center">
                    <h1 class="text-white">Matches et Résultats</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container py-5">
            <div class="row">
                @foreach($nextmatch as $game)
                    <div class="col-lg-12 m-auto">
                        <div class="widget-next-match">
                            <div class="widget-title  text-center">
                                <h2>Prochaine  match</h2>
                            </div>

                            <div class="widget-body mb-1">
                                <div class="widget-vs">
                                    <div class="d-flex align-items-center justify-content-around justify-content-between w-100">
                                        <div class="team-1 text-center">
                                            <img src="images/logo_1.png" alt="Image">
                                            <h3>{{ $game->team1->name }}</h3>
                                        </div>
                                        <div>
                                            <span class="vs"><span>VS</span></span>
                                        </div>
                                        <div class="team-2 text-center">
                                            <img src="images/logo_2.png" alt="Image">
                                            <h3>{{ $game->team2->name }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center widget-vs-contents mb-1">
                                <p class="mb-2">
                                    <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('d-m-Y') }}</span>
                                    <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('H:i') }}</span>
                                </p>
                            </div>

                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div> <!-- .site-section -->
    <div class="container py-2 ">
    <div class="row">
        <div class="col-lg-6 my-5">
            <div class="title-section">
                <div class="heading">
                    <h3>Matchs suivants </h3>
                </div>
            </div>
            <div class="row main_res">
        @foreach($games as $game)
                <div class="col-lg-12 m-auto">
                <div class="widget-body bg-light">
                    <div class="widget-vs">
                        <div class="d-flex align-items-center justify-content-around justify-content-between w-100">
                            <div class="team-1 text-center">
                                <img src="images/logo_1.png" alt="Image" class="img-fluid" width="75">
                                <h3>{{ $game->team1->name}}</h3>
                            </div>
                            <div class="text-center">
                                <span class="vs"><span>VS</span></span>
                                <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('Y-m-d') }}</span>
                                <span class="d-block">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $game->start_time)->format('H:i') }}</span>
                            </div>
                            <div class="team-2 text-center">
                                <img src="images/logo_2.png" alt="Image"  width="75">
                                <h3>{{ $game->team2->name}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        @endforeach
            </div>
    </div>
        <div class="col-lg-6 my-5">
        <div class="title-section">
            <div class="heading">
                <h3>Derniers résultats </h3>
            </div>
        </div>
        <div class="row main_res ">
            @foreach($results as $game)
            <div class="col-lg-12 m-auto ">
                <div class="d-flex team-vs">
                    <span class="score">{{ $game->result1 }} - {{ $game->result2 }}</span>
                    <div class="team-1 w-50">
                        <div class="team-details w-100 text-center">
                            <img src="images/logo_1.png" alt="Image" class="img-fluid">
                            <h3>{{ $game->team1->name }}</h3>
                        </div>
                    </div>
                    <div class="team-2 w-50">
                        <div class="team-details w-100 text-center">
                            <img src="images/logo_2.png" alt="Image" class="img-fluid">
                            <h3>{{ $game->team2->name }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    </div>
    </div>








    @include('footer')



</div>
<!-- .site-wrap -->

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.mb.YTPlayer.min.js"></script>


<script src="js/main.js"></script>

</body>

</html>