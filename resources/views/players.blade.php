<!DOCTYPE html>
<html lang="en">

<head>
    <title>Joueurs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">



</head>

<body>

<div class="site-wrap">

    @include('header')

    <div class="hero overlay mb-5" style="background-image: url('images/bg_3.jpg');">
        <div class="container">
            <div class="row align-items-center ">
                <div class="col-lg-5 mx-auto text-center">
                    <h1 class="text-white">Buteurs</h1>
                    <p>Liste des meilleurs buteurs du tournois </p>
                </div>
            </div>
        </div>
    </div>
<div class="container">
    <div class="col-lg-12 m-auto ">

        <div class="widget-next-match mb-5">
            <table class="table custom-table">
                <thead>
                <tr>
                    <th class="text-center">Nom</th>
                    <th class="text-center">Prénom</th>
                    <th class="text-center">Buts</th>
                    <th class="text-center">Passe décisif</th>
                    <th class="text-center">Carton jaune</th>
                    <th class="text-center">Carton rouge</th>
                    <th class="text-center">Equipe</th>
                </tr>
                </thead>

                    <tbody>
                    @foreach($players as $player)
                    <tr>
                        <td class="text-center"><strong class="text-white">{{$player->name}}</strong></td>
                        <td class="text-center"><strong class="text-white">{{$player->surname}}</strong></td>
                        <td class="text-center"><strong class="text-white">{{$player->buts}}</strong></td>
                        <td class="text-center"><strong class="text-white">{{$player->assist}}</strong></td>
                        <td class="text-center"><strong class="text-white">{{$player->carton_jaune}}</strong></td>
                        <td class="text-center"><strong class="text-white">{{$player->carton_rouge}}</strong></td>
                        <td class="text-center"><strong class="text-white">{{$player->team->name}}</strong></td>
                    </tr>
                        @endforeach

                    </tbody>
            </table>
        </div>

    </div>
</div>








   @include('footer')



</div>
<!-- .site-wrap -->

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.mb.YTPlayer.min.js"></script>


<script src="js/main.js"></script>

</body>

</html>