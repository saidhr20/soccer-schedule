<?php

use Illuminate\Database\Seeder;

class GroupeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now=\Carbon\Carbon::now()->toDateTimeString();
        $items = [

            ['id' => 1, 'name' => 'A', 'slugs' => 'a' , 'created_at' => $now , 'updated_at'=>$now],
            ['id' => 2, 'name' => 'B', 'slugs' => 'b' , 'created_at' => $now , 'updated_at'=>$now],
            ['id' => 3, 'name' => 'C', 'slugs' => 'c' , 'created_at' => $now , 'updated_at'=>$now],
            ['id' => 4, 'name' => 'D', 'slugs' => 'd' , 'created_at' => $now , 'updated_at'=>$now],
            ['id' => 5, 'name' => 'E', 'slugs' => 'e' , 'created_at' => $now , 'updated_at'=>$now],

        ];

        foreach ($items as $item) {
           \App\Groupe::create($item);
        }
    }
}
