<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameGroupeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_groupe', function (Blueprint $table)
        {
            $table->increments('id');
        $table->integer('game_id')->unsigned()->nullable();
        $table->foreign('game_id')->references('id')
            ->on('games')->onDelete('cascade');

        $table->integer('groupe_id')->unsigned()->nullable();
        $table->foreign('groupe_id')->references('id')
            ->on('groupe')->onDelete('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_groupe');
    }
}
