<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupeTeamsMatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groupe_team', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('team_id')->unsigned()->nullable();
            $table->foreign('team_id')->references('id')
                ->on('teams')->onDelete('cascade');


            $table->integer('groupe_id')->unsigned()->nullable();
            $table->foreign('groupe_id')->references('id')
                ->on('groupe')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groupe_team');
    }
}
