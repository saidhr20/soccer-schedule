<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groupegame extends Model
{
    protected $table="game_groupe";
    protected $fillable =['game_id','groupe_id'];
}
