<?php

namespace App\Http\Controllers\Admin;

use App\Groupe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreGamesRequest;
use App\Http\Requests\Admin\UpdateGamesRequest;

class GroupeController extends Controller
{
    /**
     * Display a listing of Game.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*if (! Gate::allows('groupe_access')) {
            return abort(401);
        }*/

        $groupe = Groupe::all();

        return view('admin.groupe.index', compact('groupe'));
    }

    /**
     * Show the form for creating new Game.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('groupe_create')) {
            return abort(401);
        }
        $name = \App\Groupe::get()->pluck('name', 'id')->prepend('Please select', '');

        return view('admin.groupe.create', compact('name', 'name'));
    }

    /**
     * Store a newly created Game in storage.
     *
     * @param  \App\Http\Requests\StoreGamesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGamesRequest $request)
    {
        if (! Gate::allows('groupe_create')) {
            return abort(401);
        }
        $groupe = Groupe::create($request->all());



        return redirect()->route('admin.groupe.index');
    }


    /**
     * Show the form for editing Game.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('groupe_edit')) {
            return abort(401);
        }
        $name = \App\Groupe::get()->pluck('name', 'id')->prepend('Please select', '');

        $groupe = Groupe::findOrFail($id);

        return view('admin.games.edit', compact('groupe', 'name'));
    }

    /**
     * Update Game in storage.
     *
     * @param  \App\Http\Requests\UpdateGamesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGamesRequest $request, $id)
    {
        if (! Gate::allows('groupe_edit')) {
            return abort(401);
        }
        $groupe = Groupe::findOrFail($id);
        $groupe->update($request->all());



        return redirect()->route('admin.groupe.index');
    }


    /**
     * Display Game.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('groupe_view')) {
            return abort(401);
        }
        $groupe = Groupe::findOrFail($id);

        return view('admin.groupe.show', compact('groupe'));
    }


    /**
     * Remove Game from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('groupe_delete')) {
            return abort(401);
        }
        $groupe = Groupe::findOrFail($id);
        $groupe->delete();

        return redirect()->route('admin.groupe.index');
    }

    /**
     * Delete all selected Game at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('groupe_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Groupe::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
