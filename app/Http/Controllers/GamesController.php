<?php

namespace App\Http\Controllers;

use App\Game;
use App\Groupe;

class GamesController extends Controller
{

    public function index()
    {
        $games = Game::whereNull('result1')->get();
        $results = Game::whereNotNull('result1')->take(4)->get();
        $nextmatch= Game::whereNull('result1')->orderby('start_time')->take(1)->get();
        $groupes = Groupe::all();
        return view('index', compact('games', 'results','groupes','nextmatch'));
    }

}
