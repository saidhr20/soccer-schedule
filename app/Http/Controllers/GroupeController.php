<?php

namespace App\Http\Controllers;

use App\Groupe;

class GroupeController extends Controller
{

    public function index()
    {
        $groupes = Groupe::all();

        return view('groupe', compact('groupes'));
    }

}
