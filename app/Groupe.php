<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groupe extends Model
{
    protected $table='groupe';
    public function team()
    {
        return  $this->belongsToMany('App\Team');
    }
    public function game()
    {
        return $this->belongsToMany('App\Game');
    }
}
