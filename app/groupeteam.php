<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groupeteam extends Model
{
    protected $table="groupe_team";
    protected $fillable =['team_id','groupe_id'];
}
